/*
 * base config for nixos-install until intial nixops deployment
 */
{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    ./hardware-config.nix
  ];

  networking = {
    hostName = "demo";
    interfaces.enp1s0.useDHCP = true;
  };

  services.openssh.enable = true;

  users.users.root = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIAwfpM32DPqH61yJFx5fNsZb654saATGr4pinQsj2VI butz@workstation"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINqBP9a/Q6SWpLBZtsQ8tqC63nxvONwoarNhpcoDwnG9 butz@notebook"
    ];
  };

  system.stateVersion = "20.09";
}
