/*
 *
 * workstation config used for the presentation
 * (nixpkgs rev. e0759a49733dfc3aa225b8a7423c00da6e1ecb67)
 *
 */
{ pkgs, ... }:

{
  boot = {
    tmpOnTmpfs = true;
    cleanTmpDir = true;
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "sd_mod" ];
      luks.devices.swap-luks = {
        device = "/dev/disk/by-uuid/aeeaebf3-2cca-4d79-85ad-891d5f9528a3";
        allowDiscards = true;
      };
    };
    kernelModules = [ "kvm-amd" ];
  };

  fileSystems = {
    "/" = {
      device = "crypt/root";
      fsType = "zfs";
    };
    "/nix" = {
      device = "crypt/nix";
      fsType = "zfs";
    };
    "/home" = {
      device = "crypt/home";
      fsType = "zfs";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/2F54-99E1";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/52dcce38-e4c9-4773-8783-ef1ab96b59c0"; }
  ];

  networking = {
    hostName = "workstation";
    hostId = "fefecafe";
    useDHCP = false;
    interfaces.enp3s0.useDHCP = true;
  };

  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  time = {
    timeZone = "Europe/Berlin";
    hardwareClockInLocalTime = true;
  };

  nixpkgs.overlays = [
    (self: super: {
      neovim = super.neovim.override {
        vimAlias = true;
        configure = {
          packages.neovim.start = [
            super.vimPlugins.vim-nix
          ];
          customRC = ''
            syntax on
          '';
        };
      };
    })
  ];

  environment = {
    systemPackages = with pkgs; [
      alacritty
      curl
      dnsutils
      exa
      fd
      firefox
      htop
      mumble
      neovim
      nixops
      ripgrep
      tree
      ungoogled-chromium
      wget
      xclip
    ];
    variables = {
      EDITOR = "vim";
    };
    shellAliases = {
      l = "exa -alhg --group-directories-first";
      ll = "exa -lg --group-directories-first";
      ls = "exa --group-directories-first";

      # only for this demonstration - please do not use this!
      ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR";
    };
  };

  programs = {
    mtr.enable = true;
    bash.enableCompletion = true;
  };

  nix.maxJobs = 16;

  services.openssh.enable = true;

  sound.enable = true;

  hardware = {
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
    };
    enableRedistributableFirmware = true;
    cpu.amd.updateMicrocode = true;
  };

  services.xserver = {
    enable = true;
    layout = "us";
    xkbOptions = "eurosign:e, grp:caps_toggle";
    xkbVariant = "altgr-intl";
    displayManager.lightdm.enable = true;
    desktopManager.plasma5.enable = true;
    exportConfiguration = true;
  };

  users.users.butz = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };

  system.stateVersion = "20.09";
}
