#!/usr/bin/env nix-shell
#!nix-shell -i bash -I g=https://github.com/nix-community/nixos-generators/archive/master.tar.gz <g> -p nixos-generators git

set -e

nixos-generate -f kexec-bundle -c bundle-assets/installer-config.nix
