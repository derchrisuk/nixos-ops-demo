{ pkgs, ... }:

{
  networking.hostName = "host-b";

  systemd.network = {
    netdevs = {
      "10-wireguard".extraConfig = ''
        [WireGuardPeer]
        PublicKey=${builtins.readFile ../secrets/wg_public_key_host-a}
        PresharedKeyFile=/var/secrets/wg_preshared_key
        AllowedIPs=fc00:2305::1/128,10.23.42.1/32
        Endpoint=host-a.some-demo.site:52342
      '';
    };
    networks = {
      "20-wireguard".extraConfig = ''
        [Match]
        Name=wg0

        [Link]
        MTUBytes=1280
        RequiredForOnline=routable

        [Address]
        Address=fc00:2305::2/128
        Peer=fc00:2305::1/128

        [Address]
        Address=10.23.42.2/32
        Peer=10.23.42.1/32
      '';
    };
  };

  services = {

    nginx = {
      enable = true;
      statusPage = true;
      recommendedTlsSettings = true;
      recommendedProxySettings = true;
      virtualHosts = {

        "some-demo.site" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          extraConfig = ''
            add_header Strict-Transport-Security "max-age=63072000; includeSubDomains" always;
          '';
          locations = {
            "/robots.txt" = {
              priority = 10;
              return = "200 \"User-agent: *\nDisallow: \n\"";
            };
            "/" = {
              priority = 20;
              return = "200 \"Oh, hi Mark!\"";
            };
          };
        };

        "monitoring.some-demo.site" = {
          enableACME = true;
          forceSSL = true;
          extraConfig = ''
            add_header Strict-Transport-Security "max-age=63072000" always;
          '';
          locations."/".proxyPass = "http://host-a-wg:3000";
        };

        "git.some-demo.site" = {
          enableACME = true;
          forceSSL = true;
          extraConfig = ''
            add_header Strict-Transport-Security "max-age=63072000" always;
          '';
          locations = {
            "/metrics" = {
              priority = 10;
              return = "403";
            };
            "/" = {
              priority = 20;
              proxyPass = "http://host-a-wg:3001";
            };
          };
        };

        "cloud.some-demo.site" = {
          enableACME = true;
          forceSSL = true;
          locations."/".proxyPass = "http://host-a-wg:80";
        };

        "chat.some-demo.site" = {
          enableACME = true;
          forceSSL = true;
          extraConfig = ''
            add_header X-Content-Type-Options "nosniff" always;
            add_header X-Frame-Options "sameorigin" always;
            add_header X-XSS-Protection "1; mode=block" always;
            add_header Strict-Transport-Security "max-age=63072000" always;
          '';
          locations = {
            "/" = {
              root = pkgs.element-web.override {
                conf = {
                  brand = "Demo Element";
                  default_theme = "dark";
                  roomDirectory.servers = [
                    "matrix.org"
                  ];
                  piwik = false;
                };
              };
            };
          };
        };
      };
    };

    prometheus.exporters = {
      nginx = {
        enable = true;
        openFirewall = true;
        firewallFilter = "-i wg0 -p tcp --dport 9113";
      };
      node = {
        openFirewall = true;
        firewallFilter = "-i wg0 -p tcp --dport 9100";
      };
    };

    journald.extraConfig = ''
      MaxFileSec=1day
      MaxRetentionSec=7day
    '';

  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  security.acme = {
    acceptTerms = true;
    email = "acme@some-demo.site";
  };

}
